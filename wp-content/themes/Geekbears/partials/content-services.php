<?php 
$first_service_title        = get_field("first_service_title");
$first_service_description  = get_field("first_service_description");
$first_service_image        = get_field("first_service_image");
$second_service_title       = get_field("second_service_title");
$second_service_description = get_field("second_service_description");
$second_service_image       = get_field("second_service_image");

?>
<section class="service-sec" id="services">
    <div class="container">
        <div class="row">
            <div class="heading text-md-center text-xs-center">
                  <h2>Services</h2>
                </div>
        </div>
    </div>  

    <div class="box">
      <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img class="wow bounceInLeft" data-wow-duration="2s" data-wow-delay="0.5s" src="<?php echo $first_service_image['url']?>" class="img-fluid" alt="<?php echo $first_service_image['alt'] ?>"/>
            </div>
            <div class="col-md-6">
                <h1><?php echo $first_service_title; ?></h1>
                <p><?php echo $first_service_description ?></p>
            </div>

        </div>

      </div>
    </div>

    <div class="box reverse">
      <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php echo $second_service_title; ?></h1>
                <p><?php echo $second_service_description ?></p>
            </div>
            <div class="col-md-6">
                <img class="wow bounceInRight" data-wow-duration="2s" data-wow-delay="1s" src="<?php echo $second_service_image['url']?>" class="img-fluid" alt="<?php echo $second_service_image['alt'] ?>" />
            </div>
        </div>

      </div>
    </div>
</section>