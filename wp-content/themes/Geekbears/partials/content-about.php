<?php
$about_us_title = get_field("about_us_title");
$about_us_image = get_field("about_us_image");
?>
<section class="service-sec" id="about">
  <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="heading text-md-center text-xs-center">
      <h2><small>About Us</small><?php echo $about_us_title; ?></h2>
    </div>
        </div>
      <div class="col-md-8">
        <div class="row">
            <div class="col-md-6 text-sm-center service-block"> <i class="fa fa-address-card" aria-hidden="true"></i>
          <h3>Better Sleep</h3>
          <p>Never in all their history have men been able truly to conceive of the world as one: a single sphere, a globe, having the qualities of a globe.</p>
        </div>
        <div class="col-md-6 text-sm-center service-block"> <i class="fa fa-bicycle" aria-hidden="true"></i>
          <h3>Reduces Weight</h3>
          <p>Never in all their history have men been able truly to conceive of the world as one: a single sphere, a globe, having the qualities of a globe.</p>
        </div>
        <div class="col-md-6 text-sm-center service-block"> <i class="fa fa-credit-card" aria-hidden="true"></i>
          <h3>Improves Mood</h3>
          <p>Never in all their history have men been able truly to conceive of the world as one: a single sphere, a globe, having the qualities of a globe.</p>
        </div>
        <div class="col-md-6 text-sm-center service-block"> <i class="fa fa-star" aria-hidden="true"></i>
          <h3>Boosts Energy</h3>
          <p>Never in all their history have men been able truly to conceive of the world as one: a single sphere, a globe, having the qualities of a globe.</p>
        </div>
        </div>
      </div>
      <div class="col-md-4"> 
        <?php if ($about_us_image): ?>
            <img src="<?php echo  $about_us_image['url'];?>" class="img-fluid"  alt="<?php echo  $about_us_image['alt'];?>"/>     
        <?php else: ?>
            <img src="<?php echo get_bloginfo("template_url")."/assets/img/blog-03.jpg"; ?>" class="img-fluid" />
        <?php endif; ?>
        
    </div>
    </div>
    <!-- /.row --> 
  </div>
</section>