<?php 
$map            = get_field("contact_us_map");
$contact_form   = get_field("contact_form");
?>


<section class="contact-sec" id="contact">
  <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2>Get in Touch <small>Our work is the presentation of our capabilities.</small> </h2>        
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-6">
            <?php if( !empty($map) ): ?>
            <div class="acf-map">
                <div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"></div>
            </div>
            <?php endif; ?>
        </div>
        <div class="col-md-12 col-lg-6">
            <?php if( !empty($contact_form) ): ?>
                <?php echo $contact_form; ?>
            <?php endif; ?>
        </div>
    </div>
  </div>
</section>