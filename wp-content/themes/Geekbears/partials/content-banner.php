<?php
$banner_title = get_field("banner_title");
$banner_image = get_field("banner_image");
?>

<div class="swiper-container main-slider" id="myCarousel">
  <div class="swiper-wrapper">
    <div class="swiper-slide slider-bg-position" style="background:url(<?php echo $banner_image['url']?>)" alt="?php echo $banner_image['alt']?>">
      <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h2>
    </div>
  </div>
</div>