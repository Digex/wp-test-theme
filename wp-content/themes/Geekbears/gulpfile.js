'use strict';

// Dependencies
var gulp = require('gulp');
var configJS = require('./config-js.json');
var configSCSS = require('./config-sass.json');
var env = process.env.NODE_ENV || 'dev';
var plugins = require('gulp-load-plugins')();
var browsersync = require('browser-sync').create();

// Tasks
gulp.task('uncache', require('./gulp-tasks/un-cache')(gulp, plugins));
gulp.task('serve', require('./gulp-tasks/serve')(gulp, plugins, browsersync));
gulp.task('sass', require('./gulp-tasks/sass')(gulp, plugins, configSCSS, env, browsersync));
gulp.task('js', require('./gulp-tasks/scripts')(gulp, plugins, configJS, env, browsersync));

// Import fontawesome fonts into proyect
gulp.task('fonts', function(){
  return gulp.src('./node_modules/@fortawesome/fontawesome-free-webfonts/webfonts/*')
  .pipe(gulp.dest('./assets/fonts'));
});

// Development Main Task Runner
gulp.task('default', ['sass', 'js', 'fonts', 'serve']);

// Production Main Task Runner
gulp.task('production', ['sass', 'js', 'fonts', 'uncache']);
