<!DOCTYPE html>
<html lang="en" ng-app="wp">
<head>
<base href="/">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="shortcut icon" type="image/png" href="<?php echo get_bloginfo("template_url")."/assets/img/favicon.png"; ?>"/>
<title>Power Fitness</title>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="loader loader-bg text-center">
    <img class="align-middle text-center loader-inner" src="<?php echo get_bloginfo("template_url")."/assets/img/ajax-document-loader.gif"; ?>">
</div>

<!-- Top Navigation -->
<div class="container">
    <div class="col-12">
        <nav class="navbar navbar-expand-md navbar-light bg-faded sticky-top">
           <a class="navbar-brand" href="<?php echo get_site_url()?>"><img src='<?php echo get_bloginfo("template_url")."/assets/img/logo.png"; ?>'></a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs4navbar" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
           </button>
           <?php
           wp_nav_menu([
             'menu'            => 'header_main_menu',
             'theme_location'  => 'header_main_menu',
             'container'       => 'div',
             'container_id'    => 'bs4navbar',
             'container_class' => 'collapse navbar-collapse',
             'menu_id'         => false,
             'menu_class'      => 'navbar-nav ml-auto',
             'depth'           => 2,
             'fallback_cb'     => 'bs4navwalker::fallback',
             'walker'          => new bs4navwalker()
           ]);
           ?>

           <!-- SOCIAL -->
            <?php get_template_part( 'partials/content', 'social' ); ?>    
           <!-- /SOCIAL -->
        </nav>

        

    </div>
</div>

