// Browsersync
module.exports = function (gulp, plugins, serve) {
  return function () {
    serve.init({
      proxy: {
        target: 'http://localhost:8080/geekbears-test/'
      }
    });
    gulp.watch("./src/js/**/*.js", ['js']);
    gulp.watch("./src/scss/**/*.scss", ['sass']);
    gulp.watch("./**/*.php").on('change', serve.reload);
  };
};
