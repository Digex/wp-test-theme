// Js
module.exports = function (gulp, plugins, config, env, serve) {
  return function () {
    gulp.src(config.scripts)
      .pipe(plugins.concat('app.min.js'))
      .pipe(plugins.if(env === 'production', plugins.uglify()))
      .pipe(gulp.dest('./assets/js'))
      .pipe(plugins.if(env === 'dev', serve.reload({ stream: true })));
  };
};