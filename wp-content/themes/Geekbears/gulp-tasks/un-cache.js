// Uncache
module.exports = function (gulp, plugins) {
  return function () {
    gulp.src('./functions.php') // Select the file we want to apply the process
      .pipe(plugins.wpRev({
        css: './assets/css/app.css',
        cssHandle: 'custom-style',
        js: './assets/js/app.min.js',
        jsHandle: 'custom-js'
      }))
      .pipe(gulp.dest('./'));
  };
};