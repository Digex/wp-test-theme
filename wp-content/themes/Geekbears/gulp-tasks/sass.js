// CSS Styling


module.exports = function (gulp, plugins, config, env, serve) {
  return function () {
      gulp.src('src/scss/app.scss')
      .pipe(plugins.if(env === 'dev', plugins.sourcemaps.init()))
      .pipe(plugins.sass({ 
          includePaths: config.styling,
          outputStyle: plugins.if(env === 'dev', 'compact', 'compressed')
        }).on('error', plugins.sass.logError))
      .pipe(plugins.if(env === 'dev', plugins.sourcemaps.write()))
      .pipe(gulp.dest('assets/css/'))
      .pipe(plugins.if(env === 'dev', serve.reload({ stream: true })));
  };
};