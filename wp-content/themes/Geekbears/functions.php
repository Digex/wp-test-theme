<?php
add_theme_support( 'post-thumbnails' );

/************************
*EXTENDS NAV
*************************/
register_nav_menus( array(
  'header_main_menu' => 'Header Menu',
  'footer_menu' => 'Footer Menu'
));

/************************
*LOAD CSS AND Js
*************************/
function theme_styles(){
  wp_register_style( 'styles', get_template_directory_uri() . '/assets/css/app.css', array(), 'v.1.0.1', 'all' );
  wp_enqueue_style( 'styles' );
  wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'theme_styles');

function theme_scripts(){
  wp_enqueue_script('theme-scripts', get_template_directory_uri() . '/assets/js/app.min.js', array('jquery'), 'v.1', true);
  wp_enqueue_script('google-scripts', 'https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCgIOxsFd5hLvuCH5RovMvYepgnwVb94gU', 'v.1', true);
  wp_localize_script('theme-scripts', 'localized',array('partials' => get_stylesheet_directory_uri() . '/angular/'));
}
add_action('wp_footer', 'theme_scripts');

/***************************
*EXTENDS NAV
****************************/
$file = TEMPLATEPATH."/inc/extends-nav.php";
if(file_exists($file)){
    require_once($file);
}

/***************************
* GOOGLE MAPS KEY
****************************/
function my_acf_google_map_api( $api ){
    
    $api['key'] = 'AIzaSyCgIOxsFd5hLvuCH5RovMvYepgnwVb94gU';
    
    return $api;
    
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

/***************************
* EXPOSE USERS FOR API
****************************/
function custom_rest_user_query( $prepared_args, $request = null ) {
  unset($prepared_args['has_published_posts']);
  return $prepared_args;
}
add_filter( 'rest_user_query' , 'custom_rest_user_query' );