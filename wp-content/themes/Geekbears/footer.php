
<footer>
  <div class="container">
    <div class="row">
     <div class="col-md-3 col-sm-6">
        <a class="navbar-brand" href="#"><img src='<?php echo get_bloginfo("template_url")."/assets/img/white-logo.png"; ?>'></a>
     </div>   
      <div class="col-md-5 col-sm-5">
        <?php
        wp_nav_menu([
          'menu'            => 'header_main_menu',
          'theme_location'  => 'header_main_menu',
          'container'       => 'div',
          'container_id'    => 'footer-nav',
          'container_class' => '',
          'menu_id'         => false,
          'menu_class'      => '',
          'depth'           => 2,
          'fallback_cb'     => 'bs4navwalker::fallback',
          'walker'          => new bs4navwalker()
        ]);
        ?>
      </div>
      <div class="col-md-4 col-sm-12">
            <!-- SOCIAL -->
            <?php get_template_part( 'partials/content', 'social' ); ?>
            <!-- /SOCIAL -->

            <a href="#" class="btn btn-transparent-white subscribe">Subscribe</a>

      </div>
    </div>
    <div class="row copy-footer">
      <div class="col-sm-12 col-md-3"> &copy;<script type="text/javascript">document.write(new Date().getFullYear());</script> Power Fitness </div>
      <div class="col-sm-12 col-md-3 pull-right text-xs-right">Made by Jorge with <i class="fa fa-heart"></i></div>
      <div class="col-sm-12 col-md-3 pull-right text-xs-right">
          <div id="google_translate_element"></div>
          <script type="text/javascript">
          function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
          }
          </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>