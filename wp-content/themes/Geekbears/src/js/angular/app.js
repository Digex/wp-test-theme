	
var app = angular.module('wp', ['ngRoute', 'ngSanitize']);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when('/', {
        templateUrl: localized.partials + 'main.html',
        controller: 'Main'
    })
})

//Main controller
app.controller('Main', ['$scope', '$http', 'usersFeed', 'instaFeed', function($scope, $http, usersFeed, instaFeed) {

    usersFeed.getUsers();
    instaFeed.getPhotos();

    console.log(instaFeed);
    $scope.users = usersFeed;
    $scope.instaPhotos = instaFeed;

}]);