function instaFeed($http) {
 
    var instaFeed = {
        photos: [],

    };

    function _setPhotos(photos) {
         instaFeed.photos = photos;
    }
 
    instaFeed.getPhotos = function() {
        return $http.get('https://api.instagram.com/v1/users/305720003/media/recent/?count=12&access_token=305720003.ebf30c3.d012f7f1a4f04685bd76f8a24ea6fbf9').then(successCallback, errorCallback);

            function successCallback(response){
                //console.log(response);
                //$scope.users = response.data;
                _setPhotos(response.data);

            }
            function errorCallback(error){
                //error code
            }
    };
 
    return instaFeed;
}
 
//Finally register the service
app.factory('instaFeed', ['$http', instaFeed]);