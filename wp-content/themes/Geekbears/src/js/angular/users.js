function usersFeed($http) {
 
    var usersFeed = {
        users: [],

    };

    function _setUsers(users) {
         usersFeed.users = users;
         /*for (var i = 0 ; i <= users.length; i++) {
             usersFeed.users.push(users[i]);
         }*/
         
    }
 
    usersFeed.getUsers = function() {
        return $http.get('/wp-json/wp/v2/users').then(successCallback, errorCallback);

            function successCallback(response){
                //console.log(response);
                //$scope.users = response.data;
                _setUsers(response.data);

            }
            function errorCallback(error){
                //error code
            }
    };
 
    return usersFeed;
}
 
//Finally register the service
app.factory('usersFeed', ['$http', usersFeed]);