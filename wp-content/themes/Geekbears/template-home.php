<?php
/*
*Template Name: Home
*/
 get_header(); 

 ?>

<!-- BANNER -->
<?php get_template_part( 'partials/content', 'banner' ); ?>
<!-- /BANNER -->

<!-- ABOUT -->
<?php get_template_part( 'partials/content', 'about' ); ?>
<!-- /ABOUT -->

<!-- SERVICES -->
<?php get_template_part( 'partials/content', 'services' ); ?>
<!-- /SERVICES -->

<!-- CONTACT -->
<?php get_template_part( 'partials/content', 'contact' ); ?>
<!-- /CONTACT -->

<!-- CONTACT -->
<?php get_template_part( 'partials/content', 'feed' ); ?>
<!-- /CONTACT -->

<?php get_footer(); ?>
