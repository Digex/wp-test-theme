# Geekbears Test
![enter image description here](https://geekbears.com/wp-content/uploads/2018/07/GB_Logo_landscape.png)
A small wordpress site that need to accomplish with the follow points

 **- 1st section**

 - Banner Content Section with a brief explanation of what does the
   client do

 **- 2nd section (About Us Page)**

- Content Section with text and photo

 **- 3rd section (Our Services)**
- A brief explanation of all the services provided by the client
The client ask us to replicate the design we have here: https://geekbears.com/services/ (we would have only 2 type of services in this case)

 **- 4th section (Contact Page)**

- Google Map with the location (you can set a random address for now)
- Contact Form with the following fields
-- Name
-- Last Name
-- Email
-- Text
-- Attachment field

 **- 5th section (Feed and users)**
- Embedded app created with Angular which will contain 2 tabs.
-- Tab 1: A list of fetched WordPress users
-- Tab 2: An Instagram feed using Angular

## Getting Started

1. Clone the repositorie
2. Install your Wordpress
3. Activate the Geekbears theme
4. Run npm install

### Prerequisites

You need to have installed node

## Running Gulp
- This project contain four different tasks that could be find in the folder gulp-tasks
- If you need to add another JS or CSS that need to be compiled you need to jadd your path in the files config-js.json/config-css.json

To run these task we have two different options
**Dev enviroment**

    npm run dev
**Prod enviroment**

    npm run prod

## Built With

* [Wordpress](https://wordpress.org/) - CMS
* [Bootstrap](https://getbootstrap.com/) - HTML DOM
* [jQuery](https://jquery.com/) - Used to add effects
* [AngularJS](https://angularjs.org/) - Used to generate the feed
*  [Gulp](https://gulpjs.com/) - minified CSS & JS

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

